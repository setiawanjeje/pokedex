import axios from 'axios'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import PokemonDetail from '../src/components/PokemonDetail'
import {capitalizeFirstLetter} from "../src/util"
import {PokemonDetail as PokemonDetailTypes} from "../src/types"

const PokemonDetailPage = () => {
  const [pokemonDetail, setPokemonDetail] = useState<PokemonDetailTypes>()
  const [isLoading, setIsLoading] = useState(false)
  const router = useRouter()
  const { pokemon } = router.query;

  const getPokemonDetail = () => {
    setIsLoading(true)
    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon}`)
    .then((res) => {
      setPokemonDetail(res.data);
    }).catch((err) => {
      console.error(err);
    })
    .finally(() => {
      setIsLoading(false);
    });
  }

  useEffect(() => {
    if (pokemon) {
      getPokemonDetail()
    }
  }, [pokemon])

  return (
    <div className='container mx-auto max-w-screen-sm pt-4 pb-20'>
      <Head>
        {pokemon && 
          <title>{capitalizeFirstLetter(String(pokemon))}</title>
        }
      </Head>
      <Link href="/">
        &larr; Back
      </Link>

      {isLoading && <div>Loading...</div>}
      
      <PokemonDetail pokemon={String(pokemon)} pokemonDetail={pokemonDetail} />

    </div>
  )

}

export default PokemonDetailPage