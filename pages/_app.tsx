import '../styles/globals.css'
import type { AppProps } from 'next/app'
import OffLineIndicator from '../src/components/OfflineIndicator'

function MyApp({ Component, pageProps }: AppProps) {
  return <>
  <Component {...pageProps} />
  <OffLineIndicator />
  </>
}

export default MyApp
