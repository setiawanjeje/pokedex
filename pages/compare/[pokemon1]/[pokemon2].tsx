import axios from 'axios'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import PokemonDetail from '../../../src/components/PokemonDetail'
import { capitalizeFirstLetter } from "../../../src/util"

const PokemonComparePage = () => {
  const [firstPokemonDetail, setFirstPokemonDetail] = useState()
  const [secondPokemonDetail, setSecondPokemonDetail] = useState()
  const [isLoading, setIsLoading] = useState(false)
  const router = useRouter()
  const { pokemon1, pokemon2 } = router.query;

  const getPokemonDetail = () => {
    setIsLoading(true)
    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon1}`)
    .then((res) => {
      setFirstPokemonDetail(res.data);
    }).catch((err) => {
      console.error(err);
    })
    .finally(() => {
    });
    axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon2}`)
    .then((res) => {
      setSecondPokemonDetail(res.data);
    }).catch((err) => {
      console.error(err);
    })
    .finally(() => {
    });
    setIsLoading(false)
  }

  useEffect(() => {
    if (pokemon1 && pokemon2) {
      getPokemonDetail()
    }
  }, [pokemon1, pokemon2])

  return (
    <div className='container mx-auto max-w-screen-sm pt-4 pb-20'>
      <Head>
        {pokemon1 && pokemon2 &&
          <title>{capitalizeFirstLetter(String(pokemon1))} vs {capitalizeFirstLetter(String(pokemon2))}</title>
        }
      </Head>
      <Link href="/">
      &larr; Back
      </Link>
      {isLoading && <div>Loading...</div>}
      {pokemon1 && pokemon2 && 
        <div className='flex gap-4'>
          <div className='flex-1'>
            <PokemonDetail pokemon={String(pokemon1)} pokemonDetail={firstPokemonDetail} />
          </div>
          <div className='flex-1'>
            <PokemonDetail pokemon={String(pokemon2)} pokemonDetail={secondPokemonDetail} />
          </div>
        </div>
      }

    </div>
  )

}

export default PokemonComparePage