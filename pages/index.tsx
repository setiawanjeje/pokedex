import type { NextPage } from 'next'
import Head from 'next/head'
import { useEffect, useRef, useState } from 'react'
import axios from 'axios'
import PokemonBox from '../src/components/PokemonBox'
import { Field, Form, Formik } from 'formik'
import { GeneralListResponse, FormCompareValues, FormFilterValues } from '../src/types'
import FilterPopup from '../src/components/FilterPopup'

const limit = 100;
const API_GET_URL_DEFAULT = `https://pokeapi.co/api/v2/pokemon/?offset=0&limit=${limit}`

const Home: NextPage = () => {
  const [pokemonList, setPokemonList] = useState<GeneralListResponse>([])
  const [numOfPokemon, setNumOfPokemon] = useState<number>(0)
  const [typeList, setTypeList] = useState<GeneralListResponse>([])
  const [generationList, setGenerationList] = useState<GeneralListResponse>([])
  const [isLoading, setIsLoading] = useState(false)
  const [isComparing, setIsComparing] = useState(false)
  const [isShowFilter, setIsShowFilter] = useState(false)
  const [nextGetApiUrl, setNextGetApiUrl] = useState(API_GET_URL_DEFAULT)
  const [offset, setOffset] = useState(0)

  const lastItemRef = useRef<HTMLDivElement | null>(null);
  const observer = useRef<IntersectionObserver>();

  useEffect(() => {
    const options = {
      root: document,
      rootMargin: "20px",
      threshold: 1
    };

    const callback = (entries: IntersectionObserverEntry[]) => {
      if (entries[0].isIntersecting && offset < numOfPokemon) {
        setOffset(num => num + limit)
      }
    };

    observer.current = new IntersectionObserver(callback, options);
    if (lastItemRef.current) {
      observer.current.observe(lastItemRef.current);
    }
    return () => {
      observer.current?.disconnect();
    };
  })

  const getPokemon = () => {
    setIsLoading(true)
    axios.get<{results: GeneralListResponse, count: number, next: string}>(nextGetApiUrl)
    .then((res) => {
      setPokemonList(list => {return [...list, ...res.data.results]});
      setNumOfPokemon(res.data.count);
      setNextGetApiUrl(res.data.next);
    }).catch((err) => {
      console.error(err);
    })
    .finally(() => {
      setIsLoading(false);
    });
  }

  const getTypes = () => {
    setIsLoading(true)
    axios.get<{results: GeneralListResponse}>(`https://pokeapi.co/api/v2/type`)
    .then((res) => {
      setTypeList(res.data.results);
    }).catch((err) => {
      console.error(err);
    })
    .finally(() => {
      setIsLoading(false);
    });
  }

  const getGeneration = () => {
    setIsLoading(true)
    axios.get<{results: GeneralListResponse}>(`https://pokeapi.co/api/v2/generation`)
    .then((res) => {
      setGenerationList(res.data.results);
    }).catch((err) => {
      console.error(err);
    })
    .finally(() => {
      setIsLoading(false);
    });
  }

  useEffect(() => {
    getTypes();
    getGeneration();
    getPokemon();
  }, [])

  useEffect(() => {
    if (numOfPokemon !== 0 && offset < numOfPokemon) {
      getPokemon();
    }
  }, [offset])


  const handleLoadMore = () => {
    console.log('handleLoadMore:')
    setOffset(prev => prev+limit)
  }

  const validateCompare = (values: FormCompareValues) => {
    const errors: {compare?: string} = {};
  
    if (!values.compare || values.compare.length === 0 || values.compare.length !== 2) {
      errors.compare = 'pls choose 2';
    }
    return errors;
  };
 
  const toggleIsComparing = () => {
    setIsComparing(prev => !prev)
  }
 
  const toggleIsShowFilter = () => {
    setIsShowFilter(prev => !prev)
  }

  const handleSubmitFilter = (values: FormFilterValues) => {
    alert(`TODO: couldn't find the API to filter pokemon
    Filter type: ${values.type}
    Filter generation: ${values.generation}`);
    toggleIsShowFilter()
  }

  return (
    <div className="pt-4 pb-20">
      <Head>
        <title>Pokedex</title>
      </Head>

      <div className='container mx-auto max-w-screen-sm relative'>
        <FilterPopup
          isShow={isShowFilter}
          toggleIsShow={toggleIsShowFilter}
          handleSubmit={handleSubmitFilter}
          typeList={typeList}
          generationList={generationList}
        />

        <div className='py-4 flex gap-4'>
          <button className={'border-2 rounded px-2 py-1'} onClick={toggleIsShowFilter}>
            Filter
          </button>
          {!isComparing &&
            <button className={'border-2 rounded px-2 py-1'} onClick={toggleIsComparing}>
              Compare
            </button>
          }
        </div>

        {isComparing ? 
        <>
          <Formik
            initialValues={{
              compare: [],
              }}
            onSubmit={async (values) => {
              location.href = `/compare/${values.compare[0]}/${values.compare[1]}`;
            }}
            validate={validateCompare}
          >
            {({errors}) => (
              <Form>
                <div className='sticky top-0 bg-white z-10 py-4'>
                  <div className='flex gap-4'>
                    <button className={'bg-white rounded px-2 py-1 border-2'} onClick={toggleIsComparing}>
                      Cancel Compare
                    </button>
                    {errors.compare ? 
                    <button className='bg-gray-400 rounded px-2 py-1 text-gray-200 cursor-not-allowed'>Let&apos;s Compare</button>
                     :
                    <button className='bg-indigo-500 rounded px-2 py-1 text-white' type='submit'>Let&apos;s Compare</button>
                    }
                  <div className='flex items-center'>
                    {errors.compare}
                  </div>
                  </div>
                </div>
                <div className='grid-cols-1 grid gap-4 mb-4'>
                {
                  pokemonList?.map((pokemon, id) => {
                    const pokeId = pokemon.url.split("/")[6];
                    return (
                      <label className='cursor-pointer relative' key={pokemon.name+id}>
                        <Field type="checkbox" name="compare" value={pokemon.name} className="absolute cursor-pointer w-4 h-4 bottom-1/2 translate-y-1/2" />
                        <div className='pl-6'>  
                          <PokemonBox name={pokemon.name} id={pokeId} />
                        </div>
                      </label>
                    )
                  })
                }
                </div>
              </Form>
            )}
          </Formik>
        </> 
        : 
        <>
          <div className='grid-cols-1 grid gap-4 mb-4'>
            {
              pokemonList?.map((pokemon, id) => {
                const pokeId = pokemon.url.split("/")[6];
                if (id === pokemonList.length - 1) {
                  return (
                    <div ref={lastItemRef}>
                      <PokemonBox name={pokemon.name} id={pokeId} key={pokemon.name+id} />
                    </div>
                  )
                }
                return (
                  <PokemonBox name={pokemon.name} id={pokeId} key={pokemon.name+id} />
                )
              })
            }
          </div>
        </>}

        <div className='flex flex-col items-center'>
          {isLoading && <div className='mb-4'>Loading...</div>}
          {/* <button className='p-2 border-2' onClick={() => {handleLoadMore()}}>Show More</button> */}
        </div>
      </div>

    </div>
  )
}

export default Home
