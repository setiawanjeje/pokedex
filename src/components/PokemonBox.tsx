import Link from "next/link";
import { capitalizeFirstLetter } from "../util";

export default function PokemonBox({name, id}: {name: string, id: string}) {
  return (
  <Link href={`/${name}`}>
    <div  className="cursor-pointer rounded-lg px-4 py-2 border-2 w-full h-30 flex items-center ">
      #{id} {capitalizeFirstLetter(name)}
    </div>
  </Link>) 
}