import { useEffect, useState } from "react";

const OffLineIndicator = () => {
  const [isOffline, setIsOffline] = useState(false)

  useEffect(() => {

    window.addEventListener('offline', function(e) { setIsOffline(true) });
    window.addEventListener('online', function(e) { setIsOffline(false) });
    
    return () => {
      window.removeEventListener('offline', function(e) { setIsOffline(true) })
      window.removeEventListener('online', function(e) { setIsOffline(false) });
    }
  }, [])


  return (
    <>
    {isOffline && 
      <div className='w-full fixed bottom-0'>
        <div className="container mx-auto py-2 max-w-screen-sm bg-white ">
          No Connection Detected
        </div>
      </div>
    }
    </>
  )
}

export default OffLineIndicator