import { Field, Form, Formik } from "formik";
import { FormFilterValues, GeneralListResponse } from "../types";
import { capitalizeFirstLetter } from "../util";

type Props = {
  isShow: boolean,
  toggleIsShow: () => void,
  handleSubmit: (val: FormFilterValues) => void
  typeList: GeneralListResponse,
  generationList: GeneralListResponse,
}

const FilterPopup = (props: Props) => {
  const {handleSubmit, isShow, toggleIsShow, typeList, generationList} = props;

  if (!isShow) return <div />

  return (
    <div className='fixed top-0 left-0 z-20 w-full bg-white '>
      <Formik
        initialValues={{
          type: [],
          generation: [],
        }}
        onSubmit={handleSubmit}
      >
        <Form>
          <div className='h-screen flex flex-col mx-auto max-w-screen-sm'>
            <div className='flex-1 overflow-y-auto'>
              <div className='py-4 flex justify-between'>
                <div>Filter</div>
                <button onClick={toggleIsShow}>Close</button>
              </div>
              <div className='flex gap-4'>
                <div className='flex-1'>
                  <div>
                    Types:
                    <div>
                      {typeList.map(type => (
                        <div className='px-2 py-1' key={type.name}>
                          <label className='cursor-pointer flex relative mr-2 items-center'>
                            <Field type="checkbox" name="type" value={type.name} />
                            <div className='pl-2'>{capitalizeFirstLetter(type.name)}</div>
                          </label>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
                <div className='flex-1'>
                  <div>
                    Generation:
                    <div>
                      {generationList.map(generation => (
                        <div className='px-2 py-1' key={generation.name}>
                          <label className='cursor-pointer flex relative mr-2 items-center' >
                            <Field type="checkbox" name="generation" value={generation.name} />
                            <div className='pl-2'>{generation.name.split("-")[1].toUpperCase()}</div>
                          </label>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='flex justify-center gap-4 py-2'>
              <button className='rounded px-2 py-1' onClick={toggleIsShow}>Cancel</button>
              <button className='bg-indigo-500 rounded px-2 py-1 text-white' type="submit">Filter</button>
            </div>
          </div>
        </Form>
      </Formik>
    </div>
  )
}

export default FilterPopup