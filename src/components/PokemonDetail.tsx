import Image from "next/image";
import {capitalizeFirstLetter} from "../util"
import {PokemonDetail as PokemonDetailTypes} from "../types"

type Props = {
  pokemon: string,
  pokemonDetail?: PokemonDetailTypes
}

export default function PokemonDetail({pokemon, pokemonDetail}: Props) {

  return (
    <div>
      <h1 className="text-xl text-center">{capitalizeFirstLetter(pokemon)}</h1>
      {pokemonDetail && 
        <>
          <div className="flex justify-center">
            <Image src={pokemonDetail.sprites.front_default} width={200} height={200} />
          </div>
          <div className="mb-4">
            Weight: {pokemonDetail.weight}
          </div>
          <div className="mb-4">
            Height: {pokemonDetail.height}
          </div>
          <div className="mb-4">
            Types:
            <div className="flex gap-2">
            {
              pokemonDetail.types.map(type => (
                <div className="px-2 rounded-full border-2" key={type.type.name}>
                  {type.type.name}{" "}
                </div>
              ))
            }
            </div>
          </div>
          <div>
            <div className="mb-2 text-center">Base Stats</div>
            {
              pokemonDetail.stats.map(eachStat => (
                <div key={eachStat.stat.name}>
                  <p> {capitalizeFirstLetter(eachStat.stat.name.replace("-", " "))}: {eachStat.base_stat}</p>
                  <Progress progress={eachStat.base_stat / 255 * 100} />
                </div>
              ))
            }
          </div>
        </>
      }
    </div>
  )
}

function Progress({progress}: {progress: number}) {
  return (
    <div style={{height: 16, width: "100%", background: "#cbd5e0"}} className="rounded relative">
      <div style={{height: 16, width: `${progress}%`, background: "#3182ce"}} className="rounded absolute"  />
    </div>
  )
}