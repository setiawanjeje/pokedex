export type GeneralListResponse = {
  name: string,
  url: string
}[];

export type FormFilterValues = {
  type: string[],
  generation: string[],
}

export type FormCompareValues = {
  compare: string[],
}

export type PokemonDetail = {
  sprites: {
    front_default: string,
  }
  weight: number,
  height: number,
  types: {
    type: {
      name: string
    }
  }[],
  stats: {
    base_stat: number,
    stat: {
      name: string
    }
  }[]
}